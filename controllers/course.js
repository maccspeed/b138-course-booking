const Course = require("../model/Course");

// Create a new course
module.exports.addCourse = async (user, reqBody) => {
	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	});

	if (user.isAdmin) {
		// Save the created object to the datebase
		return newCourse.save().then((course, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	}
	else {
		return ("You have no access");
	}
}

// Controller method for retrieving all the courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

// Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

// Retrieving a specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

// Update a course
module.exports.updateCourse = async (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		// Specify the fields/properties of the document to be updated
		let updatedCourse = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	}
	else {
		return ("You have no access");
	}
}



// Archive a course
module.exports.archiveCourse = async (user, reqParams, reqBody) => {
	if (user.isAdmin) {
		// Specify the fields/properties of the document to be updated
		let archiveCourse = {
			isActive: reqBody.isActive
		}

		return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
			if (error) {
				return false;
			}
			else {
				return true;
			}
		})
	}
	else {
		return ("You have no access");
	}
}